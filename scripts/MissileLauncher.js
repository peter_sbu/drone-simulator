import Missile from "./Missile.js";



export default class BulletLauncher{
    missiles = [];
    delayBeforeNextBullet = 0;

    constructor(canvas){
        this.canvas = canvas;
    }

    launch(missileX, missileY, speed, delay){
        const droneMissile = new Missile (missileX, missileY, speed);
        if(this.delayBeforeNextBullet <= 0){
            this.missiles.push(droneMissile);
            this.delayBeforeNextBullet = delay;
            droneMissile.sound();
        }

        this.delayBeforeNextBullet--;
    }

    draw(context){
        this.missiles.forEach((missile) => missile.draw(context));
    }
}