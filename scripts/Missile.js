export default class Missile{
    constructor(xAxis, yAxis, speed){
        this.xAxis = xAxis;
        this.yAxis = yAxis;
        this.speed = speed;

        this.width = 10;
        this.height = 10;
        this.color = 'orange';
        this.radius = 5;
        this.angle = 0;
        
    }

    draw(context){
        
        this.yAxis -= this.speed;
        //context.fillRect(this.xAxis, this.yAxis, this.width, this.height);
        context.fillStyle = this.color;
        context.beginPath();
        context.arc(this.xAxis, this.yAxis,this.radius,this.angle,2*Math.PI);
        context.fill();
    }

    sound(){
        this.sound = new Audio();
        this.sound.src = "./sounds/explosion.mp3"
        this.sound.play();
    }
}