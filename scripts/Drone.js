
export default class Drone {
    constructor(xAxisPosition, yAxisPosition, missileLauncher, canvas) {
    this.xAxisPosition = xAxisPosition;
    this.yAxisPosition = yAxisPosition;
    this.missileLauncher = missileLauncher;
    this.canvas = canvas;
    this.width = 50;
    this.height = 50;
    this.speed = 2;
    this.rotationDegrees = 90;
    this.image = new Image();
    this.image.src = "images/drone.png";

    document.addEventListener("keydown", this.keyDown);
    document.addEventListener("keyup", this.keyUP);
    }

    draw(context){
        this.move(context);
        if(this.placeDrone){
          context.drawImage(this.image, this.xAxisPosition, this.yAxisPosition, this.width, this.height);
         
          if(this.displayCurrentPosition){
            this.getCoordinates(context);
          }

          this.shoot();
        }      
    }
  
  shoot(){
    if(this.fire){
     const speed = 2;
     const delay = 7;

     let missileX = this.xAxisPosition + this.width/2;
     let missileY = this.yAxisPosition;
     this.missileLauncher.launch(missileX, missileY, speed, delay);

    }
  }

    move(context){
      this.isOutOfBoundry();
      
      if(this.moveUp){
          this.yAxisPosition -= this.speed;
      }
      if(this.moveRight){
          this.xAxisPosition += this.speed;
      }
      if(this.moveDown){
          this.yAxisPosition += this.speed;
      }
      if(this.moveLeft){
          this.xAxisPosition -= this.speed;
      }
      if(this.rotateDrone){
        this.rotate(context,this.rotationDegrees);
      }
    }

    keyDown = (event) =>{
        switch(event.key) {       
            case 'w':       
              this.moveUp = true;
              break; 
            case 'd':
              this.moveRight = true;
              break;
            case 's':
                this.moveDown = true;
              break;
            case 'a':
              this.moveLeft = true;
              break;
            case 'Enter':
              this.fire = true;
              break;
            case 'ArrowRight':
              this.rotateDrone = true;
              break;
            case 'r':
              this.displayCurrentPosition = true;
              break;
            case 'p':
                this.placeDrone = true;
                break;
        }
    }

    keyUP = (event) => {
        switch(event.key) {
          case 'w':       
            this.moveUp = false;
          break; 
          case 'd':
            this.moveRight = false;
          break;
          case 's':
              this.moveDown = false;
          break;
          case 'a':
            this.moveLeft = false;
          break;
          case 'Enter':
            this.fire = false;
          break;
        }
    }
    isOutOfBoundry(){
      let maximumXAxis = this.canvas.width - this.width;
      let maximumYAxis = this.canvas.height - this.height;
      let minimumXAxis = 0;
      let minimumYAxis = 0;

      if(this.xAxisPosition  < minimumXAxis) {
          this.xAxisPosition = minimumXAxis;
  
           return this.xAxisPosition;
      };
  
      if(this.yAxisPosition < minimumYAxis) {
          this.yAxisPosition = minimumYAxis;
  
          return this.yAxisPosition; 
      }
  
      if(this.xAxisPosition > maximumXAxis) {
          this.xAxisPosition = maximumXAxis;
          
           return this.xAxisPosition;
      };
  
      if(this.yAxisPosition > maximumYAxis) {
          this.yAxisPosition = maximumYAxis;
  
          return this.yAxisPosition; 
      }
  }

  getCoordinates(context){ 
    context.font = "15px Arial";
    context.fillStyle = "rgb(212, 146, 4)";
    context.fillText(`Coordinates: ${this.xAxisPosition},${this.yAxisPosition}`, canvas.width - 150, canvas.height - 580);
  }

  //Rotation attempt
  rotate(context,rotationDegrees){
    context.save();
    context.translate(this.xAxisCoordinate, this.yAxisCoordinate);
    context.rotate((Math.PI /180) * rotationDegrees);
    context.drawImage(this.image, this.width / -2, this.height / -2, this.width, this.height);
    context.restore();
  }
}