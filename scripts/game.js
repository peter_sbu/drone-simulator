import Drone from "./Drone.js";
import MissileLauncher from "./MissileLauncher.js";

const canvas = document.getElementById('canvas');
const context = canvas.getContext('2d');
const background = new Image();
background.src = "../images/galaxy.jpg";
canvas.width = 600;
canvas.height = 600;

const xAxisPosition = canvas.width - canvas.width;
const yAxisPosition = canvas.height - 50;

const missileLauncher = new MissileLauncher(canvas);
const drone = new Drone(xAxisPosition, yAxisPosition,missileLauncher, canvas);


function gameLoop(){
context.drawImage(background, 0, 0, canvas.width, canvas.height);
missileLauncher.draw(context);
drone.draw(context);
}

setInterval(gameLoop, 1000/60); 
