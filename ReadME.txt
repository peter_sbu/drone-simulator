*******************Toy Drone Simulator (powered by HTML5 and Javascript)
*
***Intro
* - The game is a simulator of a toy drone that is moving around a surface.
* - The drone has a shooting functionality. Shoots bullets that are delayed by a few seconds per firing.
* - The movement is controlled to prevent the drone from crossing the surface boundries.
* - The drone commands get activated only after the drone has been placed on the surface thus only the place command is active.
*
****Controls/Commands
*Controls are handled through keyboard keys.
* - p: places the drone on the surface.
* - r: displays the current coordinates of the drone's position.
* - enter: attack or shoot
****Movement*
* - w: move up
* - d: move right
* - s: move down
* - a: move left